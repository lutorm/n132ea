# N132EA, LLC, Aircraft Operations policy

## Introduction

This document contains the policy governing the operation of aircraft belonging to N132EA, LLC. Changes to this document may be made at any time by a majority vote of the members. The most recent version of this policy is always available at \url{url}.

## Pilot Requirements

1. Only members may act as Pilot In Command (PIC) of the aircraft. The only exception is dual instruction of members by non-member Certified Flight Instructors listed below.
1. Members who are CFI's may only instruct other members in the aircraft.
1. No one may act as PIC unless they are named in the aircraft insurance policy.
1. No one may operate an aircraft unless they can legally do so under the FARs.
1. Aircraft may only be operated under Part 91 and will not be used for air taxi, charter, or other commercial purposes.
1. Do we want to require some sort of currency?


### Approved CFIs
The following (non-member) CFIs are allowed to conduct dual instruction of members in LLC aircraft:

1. None

## Aircraft Scheduling

1. Aircraft scheduling is done on a first-come, first-served basis done through Google Calendar, and may be cancelled at any time.
1. There are no minimum or maximum limits on the time for which a member may schedule an aircraft, except that all members must have reasonable access to the aircraft.
1. If a member will not utilize the aircraft as scheduled, the reservation should be cancelled as soon as this is known.
1. If a member is unable to return the aircraft before the end of their reservation, they should extend their reservation as soon as this is known. If the aircraft is already scheduled during that time, the next scheduled user must be notified. At all times, safety concerns should take precedence over scheduling.
1. Unplanned maintenance needs take precedence over scheduled use, and should be indicated on the schedule.

## Usage Fees

As of May 2017, the costs to each member are:

1. A monthly fee of $X, regardless of aircraft usage, to cover fixed costs. 
1. An aircraft usage fee of $Y per hour of aircraft use, as measured by Hobbs meter. The hourly fee is ``wet'', i.e. costs of fuel, oil, and other consumables are paid for by the LLC.

At ITO, fuel can be charged to the LLC's account with Air Service Hawaii. Each member will also be provided with a credit card that may be used to purchase fuel, oil, and other consumables at other airports. 

## Operations

1. Operation must be conducted in accordance with the FARs and the POH for the respective aircraft.
1. The aircraft log book, located in the aircraft, must be reviewed before each use of the aircraft. 
1. The Hobbs start and stop time must be recorded in the aircraft log book. Any discrepancy between the start time and previous stop time should be noted as a separate entry and marked.
1. Do we want to require people to refuel after use or are there instances where people may not want full tanks for W\&B concerns?
1. Any problems must be noted in the aircraft log book, and any issue that might make the aircraft not in condition for safe operation must be brought to the attention of all members.
1. Engine oil level must be checked before each operation of the aircraft, and added as needed. A notation in the log book must be made whenever oil is added indicating the amount of oil added, the type of oil, the level prior to adding oil.

## Maintenance

1. Minor maintenance can be done directly by each member, up to a cost of $Z, but any issue affecting safety of flight must be brought to the attention of all members.
1. It is expected that all members will help out with aircraft maintenance and repairs to keep labor costs minimal. 
1. In the event of aircraft damage requiring replacement parts whose cost exceed the insurance deductible, the members will decide by majority vote whether to file an insurance claim or fund the repair with LLC funds.
1. A member will not be personally responsible for damage occuring while an aircraft was checked out by the member unless the flight was conducted in violation of this policy or the FARs.

